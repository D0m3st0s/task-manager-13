package ru.shumov.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Session;

import java.util.Collection;
import java.util.List;

public interface SessionRepository {
    @Select("SELECT * FROM sessions")
    @Results(id = "sessions", value = {
            @Result(column = "id",property = "id"),
            @Result(column = "timestamp",property = "timestamp"),
            @Result(column = "signature",property = "signature"),
            @Result(column = "user_id",property = "userId"),
            @Result(column = "owner_id",property = "ownerId")
    })
    List<Session> findAll();
    @Select("SELECT * FROM sessions WHERE id=#{id}")
    @Results(id = "session", value = {
            @Result(column = "id",property = "id"),
            @Result(column = "timestamp",property = "timestamp"),
            @Result(column = "signature",property = "signature"),
            @Result(column = "user_id",property = "userId"),
            @Result(column = "owner_id",property = "ownerId")
    })
    Session findOne(@NotNull String id);
    @Insert("INSERT INTO sessions (id,timestamp,signature,user_id,owner_id) VALUES (#{id}, #{timestamp}, #{signature}, #{userId}, #{ownerId})")
    void persist(@NotNull Session session);
    @Update("UPDATE sessions SET timestamp=#{timestamp}, signature=#{signature}, user_id=#{userId}, owner_id=#{ownerId}")

    void merge(@NotNull Session session);
    @Delete("DELETE FROM sessions WHERE id=#{id}")

    void remove(@NotNull String id);
    @Delete("DELETE FROM sessions")
    void removeAll();

}
