package ru.shumov.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Task;


import java.util.List;

public interface TaskRepository {
    @Select("SELECT * FROM tasks WHERE user_id=#{id}")
    @Results(id = "tasks", value = {
            @Result(column = "id",property = "id"),
            @Result(column = "user_id",property = "userId"),
            @Result(column = "project_id",property = "projectId"),
            @Result(column = "description",property = "description"),
            @Result(column = "status",property = "status"),
            @Result(column = "creating_date",property = "creatingDate"),
            @Result(column = "start_date",property = "startDate"),
            @Result(column = "end_date",property = "endDate"),
            @Result(column = "name",property = "name")
    })
    List<Task> findAll(@NotNull String id);
    @Select("SELECT * FROM tasks WHERE id=#{id} AND user_id=#{userId}")
    @Results(id = "task", value = {
            @Result(column = "id",property = "id"),
            @Result(column = "user_id",property = "userId"),
            @Result(column = "project_id",property = "projectId"),
            @Result(column = "description",property = "description"),
            @Result(column = "status",property = "status"),
            @Result(column = "creating_date",property = "creatingDate"),
            @Result(column = "start_date",property = "startDate"),
            @Result(column = "end_date",property = "endDate"),
            @Result(column = "name",property = "name")
    })
    Task findOne(@Param(value = "id") String id, @Param(value = "userId") String userId);
    @Insert("INSERT INTO tasks (id,user_id,project_id,description,status,creating_date,start_date,end_date,name)" +
            " VALUES (#{id}, #{userId}, #{projectId}, #{description}, #{status}, #{creatingDate}, #{startDate}, #{endDate}, #{name})")
    void persist(Task task);
    @Update("UPDATE tasks SET user_id=#{userId}, project_id=#{projectId}, description=#{description}, " +
            "status=#{status}, creating_date=#{creatingDate}, start_date=#{startDate}, end_date=#{endDate}, name=#{name}, WHERE id=#{id}")
    void merge(Task task);
    @Delete("DELETE FROM tasks WHERE id=#{id}")
    void remove(String id);
    @Delete("DELETE FROM tasks")
    void removeAll();

}
