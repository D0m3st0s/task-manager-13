package ru.shumov.tm.repository;

import org.apache.ibatis.annotations.*;
import ru.shumov.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface ProjectRepository {

    @Select("SELECT * FROM projects WHERE id=#{id} AND user_id=#{userId}")
    @Results(id = "project", value = {
            @Result(column = "id", property = "id", id = true),
            @Result(column = "user_id", property = "userId"),
            @Result(column = "name",property = "name"),
            @Result(column = "description",property = "description"),
            @Result(column = "status",property = "status"),
            @Result(column = "creating_date",property = "creatingDate"),
            @Result(column = "start_date",property = "startDate"),
            @Result(column = "end_date",property = "endDate")
    })
    Project findOne(@Param(value = "id") String id, @Param(value = "userId") String userId);
    @Select("SELECT * FROM projects WHERE user_id=#{id}")
    @Results(id = "projects", value = {
            @Result(column = "id", property = "id", id = true),
            @Result(column = "user_id", property = "userId"),
            @Result(column = "name",property = "name"),
            @Result(column = "description",property = "description"),
            @Result(column = "status",property = "status"),
            @Result(column = "creating_date",property = "creatingDate"),
            @Result(column = "start_date",property = "startDate"),
            @Result(column = "end_date",property = "endDate")
    })
    List<Project> findAll(String id);
    @Insert("INSERT INTO projects (id,user_id,name,description,status,creating_date,start_date,end_date) " +
            "VALUES (#{id}, #{userId}, #{name}, #{description}, #{status}, #{creatingDate}, #{startDate}, #{endDate})")
    void persist(Project project);
    @Update("UPDATE projects SET user_id=#{userId}, name=#{name}, description=#{description}, " +
            "status=#{status}, creating_date=#{creatingDate}, start_date=#{startDate}, end_date=#{endDate} WHERE id=#{id}")
    void merge(Project project);

    @Delete("DELETE FROM projects WHERE id=#{id}")
    void remove(String id);

    @Delete("DELETE FROM projects")
    void removeAll();

}
