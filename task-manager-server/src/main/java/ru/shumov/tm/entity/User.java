package ru.shumov.tm.entity;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import ru.shumov.tm.enums.Role;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@XmlRootElement
public class User implements Serializable {

    private String role;
    private String password;
    private String login;
    private String id = UUID.randomUUID().toString();

    @Override
    public String toString() {
        return "User{" +
                "role=" +
                ", login='" + login + '\'' + id +
                '}';
    }
}

