package ru.shumov.tm.exceptions;

public class EndpointException extends Exception{

    public EndpointException() {
        super("Service not initialized");
    }
}
