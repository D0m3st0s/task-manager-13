package ru.shumov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.repository.UserRepository;
import ru.shumov.tm.repository.factory.SqlSessionFactory;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static jakarta.xml.bind.JAXBContext.newInstance;

public class UserServiceImpl implements UserService {
    private final SqlSessionFactory sqlSessionFactory = new SqlSessionFactory();
    private final Md5Service md5Service;

    public UserServiceImpl(Md5Service md5Service) {
        this.md5Service = md5Service;
    }

    public Collection<User> getList() {
        List<User> users = new ArrayList<>();
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserRepository userMapper = sqlSession.getMapper(UserRepository.class);
            users = userMapper.findAll();
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return users;
    }

    public User getOne(@NotNull String login) {
        User user = new User() ;
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserRepository userMapper = sqlSession.getMapper(UserRepository.class);
            user = userMapper.findOne(login);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return user;
    }

    public void create(@NotNull String login, String userPassword) {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        try {
            String password = md5Service.md5(userPassword);
            user.setPassword(password);
        }
        catch (NoSuchAlgorithmException e) {throw new RuntimeException(e);}
        user.setRole(Role.USER.toString());
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserRepository userMapper = sqlSession.getMapper(UserRepository.class);
            userMapper.persist(user);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    public void update(@NotNull User user) {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserRepository userMapper = sqlSession.getMapper(UserRepository.class);
            userMapper.merge(user);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void passwordUpdate(User user, String password) {
        var md5Password = md5Service.md5(password);
        user.setPassword(md5Password);
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            UserRepository userMapper = sqlSession.getMapper(UserRepository.class);
            userMapper.merge(user);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }
}
