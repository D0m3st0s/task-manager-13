package ru.shumov.tm.service;


import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.repository.ProjectRepository;
import ru.shumov.tm.repository.TaskRepository;
import ru.shumov.tm.repository.factory.SqlSessionFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class ProjectServiceImpl implements ProjectService {
    private final SqlSessionFactory sqlSessionFactory = new SqlSessionFactory();
    public void create(@NotNull Project project) {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project name");
            }
            ProjectRepository projectMapper = sqlSession.getMapper(ProjectRepository.class);
            projectMapper.persist(project);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    public void clear() {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            ProjectRepository projectMapper = sqlSession.getMapper(ProjectRepository.class);
            projectMapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    public void remove(@Nullable String projectId, String userId) {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            ProjectRepository projectMapper = sqlSession.getMapper(ProjectRepository.class);
            TaskRepository taskMapper = sqlSession.getMapper(TaskRepository.class);
            if (projectId == null || projectId.isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project id");
            }
            List<Task> tasks = taskMapper.findAll(userId);
            for(Task task : tasks) {
                if(task.getProjectId().equals(projectId)) {
                    taskMapper.remove(task.getId());
                    sqlSession.commit();
                }
            }
            projectMapper.remove(projectId);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }
    public List<Project> getList(String id) {
        List<Project> projects = new ArrayList<>();
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            ProjectRepository projectMapper = sqlSession.getMapper(ProjectRepository.class);
            projects = projectMapper.findAll(id);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return projects;
    }

    public Project getOne(@NotNull String id, String userId) {
        Project project = new Project();
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            ProjectRepository projectMapper = sqlSession.getMapper(ProjectRepository.class);
            project = projectMapper.findOne(id, userId);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return project;
    }

    public void update(@NotNull Project project) {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project name");
            }
            ProjectRepository projectMapper = sqlSession.getMapper(ProjectRepository.class);
            projectMapper.merge(project);
            sqlSession.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    public List<Project> getSortedList(String id, String method) {
        List<Project> projects = new ArrayList<>();
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            ProjectRepository projectMapper = sqlSession.getMapper(ProjectRepository.class);
            projects = projectMapper.findAll(id);
            sorting(method, projects);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return projects;
    }
    private void sorting(@NotNull final String method, @NotNull final List<Project> projects) {
        switch (method) {
            case "by creating date":
                projects.sort(Comparator.comparing(Project::getCreatingDate));
                break;
            case "by start date":
                projects.sort(Comparator.comparing(Project::getStartDate));
                break;
            case "by end date":
                projects.sort(Comparator.comparing(Project::getEndDate));
                break;
            case "by status":
                projects.sort(Comparator.comparing(Project::getStatus));
                break;
        }
    }
    public List<Project> find(String id, String part){
        List<Project> projects = new ArrayList<>();
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            ProjectRepository projectMapper = sqlSession.getMapper(ProjectRepository.class);
            List<Project> values = projectMapper.findAll(id);
            for (Project project : values) {
                if (project.getName().contains(part) || project.getDescription().contains(part)) {
                    projects.add(project);
                }
            }
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return projects;
    }

}
