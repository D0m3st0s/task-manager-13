package ru.shumov.tm.service;

import lombok.SneakyThrows;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Connector {
    public static void registerDriverJDBC() {
        try {
            final Driver driver = new com.mysql.jdbc.Driver();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    public static Connection connectionDB() {
        registerDriverJDBC();
        Connection connection;
        final File workingFolder = new File("task-manager-server"+File.separator+"src"+File.separator+
                "main"+File.separator+ "resources");
        final File properties = new File(workingFolder, "ConnectionDB.properties");
        final FileInputStream fileInputStream = new FileInputStream(properties);
        final Properties property = new Properties();
        property.load(fileInputStream);
        final String login = property.getProperty("db.login");
        final String password = property.getProperty("db.password");
        final String host = property.getProperty("db.host");
        connection = DriverManager.getConnection(host, login, password);
        return connection;
    }
}
