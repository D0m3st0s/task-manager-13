package ru.shumov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.repository.TaskRepository;
import ru.shumov.tm.repository.factory.SqlSessionFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskServiceImpl implements TaskService {
    private final SqlSessionFactory sqlSessionFactory = new SqlSessionFactory();
    public void create(@NotNull Task task) {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            TaskRepository taskMapper = sqlSession.getMapper(TaskRepository.class);
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "project id");
            }
            taskMapper.persist(task);
            sqlSession.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    public void clear() {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            TaskRepository taskMapper = sqlSession.getMapper(TaskRepository.class);
            taskMapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    public void remove(@NotNull String taskId) {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            TaskRepository taskMapper = sqlSession.getMapper(TaskRepository.class);
            taskMapper.remove(taskId);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    public List<Task> getList(@NotNull String id) {
        List<Task> tasks = new ArrayList<>();
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            TaskRepository taskMapper = sqlSession.getMapper(TaskRepository.class);
            tasks = taskMapper.findAll(id);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    public List<Task> getSortedList(String id, String method) {
        List<Task> tasks = new ArrayList<>();
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            TaskRepository taskMapper = sqlSession.getMapper(TaskRepository.class);
            tasks = taskMapper.findAll(id);
            sqlSession.commit();
            sorting(method, tasks);
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    public Task getOne(@NotNull String id, String userId) {
        Task task = new Task();
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            TaskRepository taskMapper = sqlSession.getMapper(TaskRepository.class);
            task = taskMapper.findOne(id, userId);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return task;
    }

    public List<Task> find(String id, String part) {
        List<Task> tasks = new ArrayList<>();
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            TaskRepository taskMapper = sqlSession.getMapper(TaskRepository.class);
            List<Task> values = taskMapper.findAll(id);
            sqlSession.commit();
            for (Task task : values) {
                if (task.getName().contains(part) || task.getDescription().contains(part)) {
                    tasks.add(task);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    public void update(@NotNull Task task) {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            if (task.getName() == null || task.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "task name");
            }
            if (task.getProjectId() == null || task.getProjectId().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_TASK + "project id");
            }
            TaskRepository taskMapper = sqlSession.getMapper(TaskRepository.class);
            taskMapper.merge(task);
            sqlSession.commit();

        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }
    public void sorting(@NotNull final String method, @NotNull final List<Task> tasks) {
        switch (method) {
            case "by creating date":
                tasks.sort(Comparator.comparing(Task::getCreatingDate));
                break;
            case "by start date":
                tasks.sort(Comparator.comparing(Task::getStartDate));
                break;
            case "by end date":
                tasks.sort(Comparator.comparing(Task::getEndDate));
                break;
            case "by status":
                tasks.sort(Comparator.comparing(Task::getStatus));
                break;
        }

    }
}
