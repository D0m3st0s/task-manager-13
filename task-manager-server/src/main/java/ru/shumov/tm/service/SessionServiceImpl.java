package ru.shumov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;

import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import ru.shumov.tm.bootstrap.Bootstrap;
import ru.shumov.tm.entity.Session;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.exceptions.ValidateException;
import ru.shumov.tm.repository.SessionRepository;
import ru.shumov.tm.repository.factory.SqlSessionFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
@NoArgsConstructor
@Getter
@Setter
public class SessionServiceImpl implements SessionService{
    private final SqlSessionFactory sqlSessionFactory = new SqlSessionFactory();
    private final String id = UUID.randomUUID().toString();
    private Bootstrap bootstrap;

    public SessionServiceImpl(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void validate(final Session session) throws ValidateException {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        SessionRepository sessionMapper = sqlSession.getMapper(SessionRepository.class);
        if(session == null) throw new ValidateException();
        if(session.getSignature() == null || session.getSignature().isEmpty()) throw new ValidateException();
        if(session.getUserId() == null || session.getUserId().isEmpty()) throw new ValidateException();
        if(session.getTimestamp() == null) throw new ValidateException();
        final Session value = session.clone();
        final String sessionSignature = session.getSignature();
        final String valueSignature = sing(value).getSignature();
        final boolean check = sessionSignature.equals(valueSignature);
        try {
            if (!check) throw new ValidateException();
            Session sessionInDB = sessionMapper.findOne(session.getId());
            sqlSession.commit();
            sqlSession.close();
            if (sessionInDB == null) throw new ValidateException();
        } catch (Exception ignored) {
        }
    }

    public Session sing(final Session session) {
        if(session == null) {return null;}
        session.setSignature(null);
        final String signature = SignatureService.sign(session, "Ништяк пацаны, сегодня кайфуем", 365);
        session.setSignature(signature);
        return session;
    }

    public Session openSession(final String login) throws ValidateException {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        Session finalSession = new Session();
        try {
            SessionRepository sessionMapper = sqlSession.getMapper(SessionRepository.class);
            final User user = bootstrap.getUserService().getOne(login);
            final Session session = new Session();
            session.setUserId(user.getId());
            session.setOwnerId(id);
            session.setTimestamp(1000L);
            finalSession = sing(session);
            if (finalSession == null) throw new ValidateException();
            sessionMapper.persist(finalSession);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return finalSession;
    }

    public void closeSession(final Session session) throws ValidateException {
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            SessionRepository sessionMapper = sqlSession.getMapper(SessionRepository.class);
            validate(session);
            sessionMapper.remove(session.getId());
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
    }

    public List<Session> getSessionList() {
        List<Session> sessions = new ArrayList<>();
        SqlSession sqlSession = sqlSessionFactory.getSqlSessionFactory().openSession();
        try {
            SessionRepository sessionMapper = sqlSession.getMapper(SessionRepository.class);
            sessions = sessionMapper.findAll();
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
            sqlSession.rollback();
        } finally {
            sqlSession.close();
        }
        return sessions;
    }
}
