
package ru.shumov.tm.endpoint.project;

import javax.xml.namespace.QName;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import ru.shumov.tm.endpoint.Session;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.shumov.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final QName _Clear_QNAME = new QName("http://endpoint.tm.shumov.ru/", "clear");
    private static final QName _ClearResponse_QNAME = new QName("http://endpoint.tm.shumov.ru/", "clearResponse");
    private static final QName _Create_QNAME = new QName("http://endpoint.tm.shumov.ru/", "create");
    private static final QName _CreateResponse_QNAME = new QName("http://endpoint.tm.shumov.ru/", "createResponse");
    private static final QName _Find_QNAME = new QName("http://endpoint.tm.shumov.ru/", "find");
    private static final QName _FindResponse_QNAME = new QName("http://endpoint.tm.shumov.ru/", "findResponse");
    private static final QName _GetList_QNAME = new QName("http://endpoint.tm.shumov.ru/", "getList");
    private static final QName _GetListResponse_QNAME = new QName("http://endpoint.tm.shumov.ru/", "getListResponse");
    private static final QName _GetOne_QNAME = new QName("http://endpoint.tm.shumov.ru/", "getOne");
    private static final QName _GetOneResponse_QNAME = new QName("http://endpoint.tm.shumov.ru/", "getOneResponse");
    private static final QName _GetSortedList_QNAME = new QName("http://endpoint.tm.shumov.ru/", "getSortedList");
    private static final QName _GetSortedListResponse_QNAME = new QName("http://endpoint.tm.shumov.ru/", "getSortedListResponse");
    private static final QName _Project_QNAME = new QName("http://endpoint.tm.shumov.ru/", "project");
    private static final QName _Remove_QNAME = new QName("http://endpoint.tm.shumov.ru/", "remove");
    private static final QName _RemoveResponse_QNAME = new QName("http://endpoint.tm.shumov.ru/", "removeResponse");
    private static final QName _Update_QNAME = new QName("http://endpoint.tm.shumov.ru/", "update");
    private static final QName _UpdateResponse_QNAME = new QName("http://endpoint.tm.shumov.ru/", "updateResponse");
    private static final QName _Exception_QNAME = new QName("http://endpoint.tm.shumov.ru/", "Exception");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.shumov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Clear }
     * 
     * @return
     *     the new instance of {@link Clear }
     */
    public Clear createClear() {
        return new Clear();
    }

    /**
     * Create an instance of {@link ClearResponse }
     * 
     * @return
     *     the new instance of {@link ClearResponse }
     */
    public ClearResponse createClearResponse() {
        return new ClearResponse();
    }

    /**
     * Create an instance of {@link Create }
     * 
     * @return
     *     the new instance of {@link Create }
     */
    public Create createCreate() {
        return new Create();
    }

    /**
     * Create an instance of {@link CreateResponse }
     * 
     * @return
     *     the new instance of {@link CreateResponse }
     */
    public CreateResponse createCreateResponse() {
        return new CreateResponse();
    }

    /**
     * Create an instance of {@link Find }
     * 
     * @return
     *     the new instance of {@link Find }
     */
    public Find createFind() {
        return new Find();
    }

    /**
     * Create an instance of {@link FindResponse }
     * 
     * @return
     *     the new instance of {@link FindResponse }
     */
    public FindResponse createFindResponse() {
        return new FindResponse();
    }

    /**
     * Create an instance of {@link GetList }
     * 
     * @return
     *     the new instance of {@link GetList }
     */
    public GetList createGetList() {
        return new GetList();
    }

    /**
     * Create an instance of {@link GetListResponse }
     * 
     * @return
     *     the new instance of {@link GetListResponse }
     */
    public GetListResponse createGetListResponse() {
        return new GetListResponse();
    }

    /**
     * Create an instance of {@link GetOne }
     * 
     * @return
     *     the new instance of {@link GetOne }
     */
    public GetOne createGetOne() {
        return new GetOne();
    }

    /**
     * Create an instance of {@link GetOneResponse }
     * 
     * @return
     *     the new instance of {@link GetOneResponse }
     */
    public GetOneResponse createGetOneResponse() {
        return new GetOneResponse();
    }

    /**
     * Create an instance of {@link GetSortedList }
     * 
     * @return
     *     the new instance of {@link GetSortedList }
     */
    public GetSortedList createGetSortedList() {
        return new GetSortedList();
    }

    /**
     * Create an instance of {@link GetSortedListResponse }
     * 
     * @return
     *     the new instance of {@link GetSortedListResponse }
     */
    public GetSortedListResponse createGetSortedListResponse() {
        return new GetSortedListResponse();
    }

    /**
     * Create an instance of {@link Project }
     * 
     * @return
     *     the new instance of {@link Project }
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link Remove }
     * 
     * @return
     *     the new instance of {@link Remove }
     */
    public Remove createRemove() {
        return new Remove();
    }

    /**
     * Create an instance of {@link RemoveResponse }
     * 
     * @return
     *     the new instance of {@link RemoveResponse }
     */
    public RemoveResponse createRemoveResponse() {
        return new RemoveResponse();
    }

    /**
     * Create an instance of {@link Update }
     * 
     * @return
     *     the new instance of {@link Update }
     */
    public Update createUpdate() {
        return new Update();
    }

    /**
     * Create an instance of {@link UpdateResponse }
     * 
     * @return
     *     the new instance of {@link UpdateResponse }
     */
    public UpdateResponse createUpdateResponse() {
        return new UpdateResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     * @return
     *     the new instance of {@link Exception }
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link Session }
     * 
     * @return
     *     the new instance of {@link Session }
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Clear }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Clear }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "clear")
    public JAXBElement<Clear> createClear(Clear value) {
        return new JAXBElement<>(_Clear_QNAME, Clear.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ClearResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "clearResponse")
    public JAXBElement<ClearResponse> createClearResponse(ClearResponse value) {
        return new JAXBElement<>(_ClearResponse_QNAME, ClearResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Create }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Create }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "create")
    public JAXBElement<Create> createCreate(Create value) {
        return new JAXBElement<>(_Create_QNAME, Create.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "createResponse")
    public JAXBElement<CreateResponse> createCreateResponse(CreateResponse value) {
        return new JAXBElement<>(_CreateResponse_QNAME, CreateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Find }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Find }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "find")
    public JAXBElement<Find> createFind(Find value) {
        return new JAXBElement<>(_Find_QNAME, Find.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "findResponse")
    public JAXBElement<FindResponse> createFindResponse(FindResponse value) {
        return new JAXBElement<>(_FindResponse_QNAME, FindResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetList }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetList }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "getList")
    public JAXBElement<GetList> createGetList(GetList value) {
        return new JAXBElement<>(_GetList_QNAME, GetList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetListResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "getListResponse")
    public JAXBElement<GetListResponse> createGetListResponse(GetListResponse value) {
        return new JAXBElement<>(_GetListResponse_QNAME, GetListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOne }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetOne }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "getOne")
    public JAXBElement<GetOne> createGetOne(GetOne value) {
        return new JAXBElement<>(_GetOne_QNAME, GetOne.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOneResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetOneResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "getOneResponse")
    public JAXBElement<GetOneResponse> createGetOneResponse(GetOneResponse value) {
        return new JAXBElement<>(_GetOneResponse_QNAME, GetOneResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSortedList }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetSortedList }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "getSortedList")
    public JAXBElement<GetSortedList> createGetSortedList(GetSortedList value) {
        return new JAXBElement<>(_GetSortedList_QNAME, GetSortedList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSortedListResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetSortedListResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "getSortedListResponse")
    public JAXBElement<GetSortedListResponse> createGetSortedListResponse(GetSortedListResponse value) {
        return new JAXBElement<>(_GetSortedListResponse_QNAME, GetSortedListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Project }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Project }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "project")
    public JAXBElement<Project> createProject(Project value) {
        return new JAXBElement<>(_Project_QNAME, Project.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Remove }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Remove }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "remove")
    public JAXBElement<Remove> createRemove(Remove value) {
        return new JAXBElement<>(_Remove_QNAME, Remove.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "removeResponse")
    public JAXBElement<RemoveResponse> createRemoveResponse(RemoveResponse value) {
        return new JAXBElement<>(_RemoveResponse_QNAME, RemoveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Update }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Update }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "update")
    public JAXBElement<Update> createUpdate(Update value) {
        return new JAXBElement<>(_Update_QNAME, Update.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "updateResponse")
    public JAXBElement<UpdateResponse> createUpdateResponse(UpdateResponse value) {
        return new JAXBElement<>(_UpdateResponse_QNAME, UpdateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<>(_Exception_QNAME, Exception.class, null, value);
    }

}
