package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.Status;
import ru.shumov.tm.endpoint.project.Project;
import ru.shumov.tm.endpoint.user.Role;


import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

public class ProjectCreateCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "project create";
    @Getter
    private final String description = "project create: Создание нового проекта.";

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (!user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        try {
            Project project = new Project();
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            var date = new Date();
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            XMLGregorianCalendar creatingDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
            project.setCreatingDate(creatingDate);
            bootstrap.getTerminalService().outPutString(Constants.ENTER_PROJECT_NAME);
            @NotNull final var name = bootstrap.getTerminalService().scanner();
            @NotNull final var userId = bootstrap.getUser().getId();
            @NotNull final var id = UUID.randomUUID().toString();
            bootstrap.getTerminalService().outPutString(Constants.ENTER_START_DATE_OF_PROJECT);
            @NotNull final var startDate = bootstrap.getTerminalService().scanner();
            calendar.setTime(format.parse(startDate));
            XMLGregorianCalendar XMLStartDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
            project.setStartDate(XMLStartDate);
            bootstrap.getTerminalService().outPutString(Constants.ENTER_DEADLINE_OF_PROJECT);
            @NotNull final var endDate = bootstrap.getTerminalService().scanner();
            calendar.setTime(format.parse(endDate));
            XMLGregorianCalendar XMLEndDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
            project.setEndDate(XMLEndDate);
            bootstrap.getTerminalService().outPutString(Constants.ENTER_DESCRIPTION_OF_PROJECT);
            @NotNull final var description = bootstrap.getTerminalService().scanner();

            project.setDescription(description);
            project.setName(name);
            project.setId(id);
            project.setUserId(userId);
            project.setStatus(Status.PLANNED);
            bootstrap.getProjectEndPoint().create(session, project);
            bootstrap.getTerminalService().outPutString(Constants.DONE);
        } catch (ParseException parseException) {
            bootstrap.getTerminalService().outPutString(Constants.INCORRECT_DATE_FORMAT);
        }
    }

    public ProjectCreateCommand() {
    }
}
