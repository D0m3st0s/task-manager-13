package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.user.Role;

public class ProjectClearCommand extends AbstractCommand {
    private final Role role = Role.ADMINISTRATOR;
    @Getter
    private final String name = "project clear";
    @Getter
    private final String description = "project clear: Удаление всех проектов.";

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ALL_PROJECTS_WILL_BE_CLEARED);
            bootstrap.getTerminalService().outPutString(Constants.YES_NO);
            @NotNull final var answer = bootstrap.getTerminalService().scanner();
            if (answer.equals(Constants.YES)) {
                bootstrap.getProjectEndPoint().clear(session);
                bootstrap.getTaskEndPoint().clear(session);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public ProjectClearCommand() {
    }
}
