package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.Status;
import ru.shumov.tm.endpoint.task.Task;
import ru.shumov.tm.endpoint.user.Role;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class TaskUpdateCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "task update";
    @Getter
    private final String description = "task update: Изменения параметров задачи.";

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        var task = new Task();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            GregorianCalendar calendar = new GregorianCalendar();
            bootstrap.getTerminalService().outPutString(Constants.ENTER_ID_OF_TASK_FOR_SHOWING);
            @NotNull final var taskId = bootstrap.getTerminalService().scanner();
            if (bootstrap.getTaskEndPoint().getOne(session, taskId) == null) {
                bootstrap.getTerminalService().outPutString(Constants.TASK_DOES_NOT_EXIST);
            } else {
                task = bootstrap.getTaskEndPoint().getOne(session, taskId);
            }
            if (!task.getUserId().equals(user.getId())) {
                bootstrap.getTerminalService().outPutString("У вас нет доступа к этой задаче.");
                return;
            }
            try {
                bootstrap.getTerminalService().outPutString(Constants.ENTER_TASK_NAME);
                @NotNull final var name = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_DESCRIPTION_OF_TASK);
                @NotNull final var description = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_START_DATE_OF_TASK);
                @NotNull final var startDate = bootstrap.getTerminalService().scanner();
                calendar.setTime(format.parse(startDate));
                XMLGregorianCalendar XMLStartDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
                task.setStartDate(XMLStartDate);
                bootstrap.getTerminalService().outPutString(Constants.ENTER_DEADLINE_OF_PROJECT);
                @NotNull final var endDate = bootstrap.getTerminalService().scanner();
                calendar.setTime(format.parse(endDate));
                XMLGregorianCalendar XMLEndDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
                task.setEndDate(XMLEndDate);
                bootstrap.getTerminalService().outPutString("Введите статус задачи:");
                bootstrap.getTerminalService().outPutString("planned/in process/done");
                @NotNull final var status = bootstrap.getTerminalService().scanner();
                switch (status) {
                    case ("planned"):
                        task.setStatus(Status.PLANNED);
                        break;
                    case ("in process"):
                        task.setStatus(Status.IN_PROCESS);
                        break;
                    case ("done"):
                        task.setStatus(Status.DONE);
                        break;
                }

                task.setName(name);
                task.setDescription(description);
                bootstrap.getTaskEndPoint().update(session, task);
                bootstrap.getTerminalService().outPutString(Constants.DONE);
            } catch (ParseException parseException) {
                bootstrap.getTerminalService().outPutString(Constants.INCORRECT_DATE_FORMAT);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public TaskUpdateCommand() {
    }
}
